let font
let s = {
  w: undefined,
  h: undefined,
  r: 16 / 9
}

let colors = [ 
  [ 103, 119, 81 ], 
  [ 81, 121, 112 ], 
  [ 54, 79, 83 ], 
  [ 47, 61, 70 ]
]

let playerL = {
  pos: undefined,
  speed: 0.05,
  joined: false
}
let playerR = {
  pos: undefined,
  speed: 0.05,
  joined: false
}
let middleButton = false

let trees = []
let treeToPlant

let sfx = {
  planted: undefined,
  failure: undefined,
  click: undefined
}

let states = {
  menu: true,
  play: false
}

let highscores = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
let score = 0
let middle = {
  x: undefined,
  y: undefined
}
let gridSize = undefined

function preload() {
  font = loadFont( 'geo.ttf' )
  sfx.planted = loadSound('planted.mp3')
  sfx.failure = loadSound('failure.mp3')
  sfx.click = loadSound('click.mp3')
}

function setup() {
  textFont( font )
  createCanvas( windowWidth, windowHeight )
  
  if ( windowWidth / windowHeight >= 1 / s.r ) {
    s.h = windowHeight
    s.w = s.h / s.r
  } else {
    s.w = windowWidth
    s.h  = s.w * s.r
  }

  middle.x = windowWidth * 0.5
  middle.y = windowHeight * 0.5
  gridSize = s.h / 32
  
  playerL.pos = 0
  playerR.pos = 0
  
  treeToPlant = new Tree()
}

function draw() {
  frameRate( 30 )
  background( 152, 160, 147 )
  
  if ( states.menu ) {
    textAlign( CENTER, CENTER )
    fill( 0 )
    noStroke()
    textSize( gridSize * 8 )
    text( 'nuft', middle.x, middle.y - gridSize * 10 )
    
    textSize( gridSize )
    textAlign( RIGHT, CENTER )
    text( 'left player\ncontrols\nthe vertical line\n\nright player\ncontrols\nthe horizontal line\n\nmiddle player\nplants\nthe tree', middle.x + gridSize, middle.y + gridSize * 4 )

    textAlign( LEFT, CENTER )
    text( highscores.join( '\n' ), middle.x + gridSize * 3, middle.y + gridSize * 4 )
  }

  if ( states.play ) { 
    for ( let tree of trees ) {
      tree.render()
    }
    treeToPlant.render()
    treeToPlant.checkCollision()
  
    noFill()
    stroke( 255, 128 )
    strokeWeight( 2 )
    let playerLPos = middle.x + playerL.pos 
    let playerRPos = middle.y + playerR.pos 
    let offset = gridSize * 0.5
    line( playerLPos, 0, playerLPos, playerRPos - offset )
    line( playerLPos, windowHeight, playerLPos, playerRPos + offset )
    line( 0, playerRPos, playerLPos - offset, playerRPos )
    line( windowWidth, playerRPos, playerLPos + offset, playerRPos )
    circle( playerLPos, playerRPos, gridSize )
  
    playerL.pos += playerL.speed * deltaTime
    playerR.pos += playerR.speed * deltaTime

    textAlign( CENTER, CENTER )
    textSize( gridSize * 2 )
    fill( 0, 128 )
    noStroke()
    text( score, middle.x, middle.y + gridSize * 14 )

    let playerLimitL = s.w * 0.5
    let playerLimitR = s.h * 0.5
    if ( playerL.pos >= playerLimitL ) {
      playerL.pos = playerLimitL
      playerL.speed *= -1
    }
    if ( playerL.pos <= -playerLimitL ) {
      playerL.pos = -playerLimitL
      playerL.speed *= -1
    }
    if ( playerR.pos >= playerLimitR ) {
      playerR.pos = playerLimitR
      playerR.speed *= -1
    }
    if ( playerR.pos <= -playerLimitR ) {
      playerR.pos = -playerLimitR
      playerR.speed *= -1
    }
  }
}

class Tree {
  constructor() {
    this.size = gridSize
    let limitX = s.w * 0.5
    let limitY = s.h * 0.5
    this.x = random( -limitX + this.size, limitX - this.size )
    this.y = random( -limitY + this.size, limitY - this.size * 4 )
    this.state = false
    this.speed = 0.5
    this.isPlanted = false
    this.color = Math.floor( Math.random() * 4 )
  }

  render() {
    if ( this.isPlanted ) {
      strokeWeight( gridSize * 0.2 )
      stroke( 128, 64, 0 )
      noFill()
      line( middle.x + this.x, middle.y + this.y, middle.x + this.x, middle.y + this.y + gridSize )
    }
    if ( this.state ) {
      noStroke()
      fill( 190, 10, 110 )
    } 
    if ( !this.state) {
      noFill()
      strokeWeight( 2 )
      stroke( 190, 10, 110 )
    }
    if ( this.isPlanted ) {
      noStroke()
      fill( colors[ this.color ] )
    }
    circle( middle.x + this.x, middle.y + this.y, gridSize )
  }

  checkCollision() {
    let y = playerR.pos - this.y
    let x = playerL.pos - this.x
    let dist = sqrt( x * x + y * y )
    if ( dist <= s.h / 32 ) {
      this.state = true
    } else {
      this.state = false
    }
  }
}

function mousePressed() {
  if ( states.menu ) {
    setTimeout(() => {
      trees = []
      treeToPlant = new Tree()
      score = 0
      playerL.pos = 0
      playerR.pos = 0
      sfx.click.play()
      states.menu = false
      states.play = true
    }, 100 )
  }
  if ( states.play ) {
    if ( mouseButton === LEFT ) playerL.speed *= -1
    if ( mouseButton === RIGHT ) playerR.speed *= -1
    if ( mouseButton === CENTER ) {
      middleButton = true
      if ( treeToPlant.state ) {
        playerL.speed = (playerL.speed > 0) ? playerL.speed += 0.0025 : playerL.speed -= 0.0025
        playerR.speed = (playerR.speed > 0) ? playerR.speed += 0.0025 : playerR.speed -= 0.0025
        treeToPlant.isPlanted = true
        trees.push( treeToPlant )
        treeToPlant = new Tree()
        score++
        sfx.planted.play()
      } else {
        setTimeout(() => {
          if ( Math.min( ...highscores ) < score ) {
            highscores.unshift( score )
            highscores.pop()
            highscores = highscores.sort( function ( a, b ) {
              return b - a
            } )
          }
          sfx.failure.play()
          states.play = false
          states.menu = true
        }, 100 )
      }
      trees = trees.sort( function ( a, b ) {
        return a.y - b.y
      } )
    }
  }
}

function windowResized() {
  createCanvas( windowWidth, windowHeight )
  if ( windowWidth / windowHeight >= 1 / s.r ) {
    s.h = windowHeight
    s.w = s.h / s.r
  } else {
    s.w = windowWidth
    s.h  = s.w * s.r
  }

  middle.x = windowWidth * 0.5
  middle.y = windowHeight * 0.5
  gridSize = s.h / 32
}
